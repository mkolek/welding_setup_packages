# Welding_Setup packages #


This repo contains the ROS software for the sound based Welding setup at AAU,  where a UR5 arm is used for robotic welding. A microphone, placed in the vicinity records the sound of the welding torch for future data processing, an Arduino is used for collecting data about the welding current and voltage (future plan). 
The idea of using ROS for the setup is that the motion of the robot, the sound data, and other relevant data can be collected in as single, synchronized file using the Rosbag utility.

All file locations are given in reference to `your_catkin_workspace/src/welding_setup_packages`.  A  `$ ...` indicates a terminal command.  

#### Status 
The project is currently under development.  Due to the corona-lockdown it has at the current moment not been tested on real hardware, but it works in rviz.  


## Package List: ##

* __aau_ur_launch__ - Is just a launch file for connecting to and controlling the real UR5 arm. 
It requires to have the _Universal_Robots_ROS_Driver_ package  (see below), and your UR robot set up properly, with the newest Polyscope (software on the UR flexpad) installed, the IP -address set up and the ROS enabling Urcap installed.  For step by step instructions see ROS driver  documentation [here](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver). 
* __audio_common__ - Is the package for capturing the sound from the microphone. 
* __audio_convert__ - Is the package for converting the rosbag files back to .wav files for extracting the captured audio.
* __fmauch_universal_robot__ - Is the package containing the _ur_description_ which is the used URDF and SRDF for the UR5 robot. 
	* _ur5_moveit_config2_ - Is a standard MoveIt setup of the ur5 robot to control it with the default OMPL planners and a python interface like described [here](http://docs.ros.org/kinetic/api/moveit_tutorials/html/doc/move_group_python_interface/move_group_python_interface_tutorial.html) . You can find a few examples of scripts for moving the robot with Python in the subfolder _ur5_moveit_config2/scripts_ . 

* __pilz_robotics__ -  As the standard MoveIt python interface does not, (as of now) provide a convenient way of achieving constant TCP-speed linear motion, the trajectory generator for the Pilz Robotics PRBT manipulator was used, and adapted to the UR5 arm. The folder contains the software needed for the trajectory generation. Original repo (with tutorials) [here](https://github.com/PilzDE/pilz_industrial_motion). 

* __Universal_Robots_ROS_Driver__ - The official ROS driver for connecting and controlling the real UR5 Arm . Setup guide [here](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver)

*  __ur_linmove__ -  As the planning pipeline for the pilz_robotics trajectory generator has a lot of hidden references to the manufacturers own manipulator this package contains the launch files for "transplanting" the pilz_robotics planner onto the ur5 arm. 
_If you want to use the linear planner on your own URDF, see more instructions in the folder._


## Setup ##

* Create a new empty catkin workspace.
* Add the packages to your workspace src folder.  Install all dependencies by going to the top directory of your workspace and run:  
 
    `rosdep install --from-paths src --ignore-src -r -y`
    
 * Build the workspace with `catkin build`, and you are ready to go!
  
* To setup the actual UR5 for being controlled via ROS, follow the  setup instructions from the [Universal_Robots_ROS_Driver]( https://github.com/UniversalRobots/Universal_Robots_ROS_Driver) documentation. 


## Data acquisition Pipeline overview

The pipeline is roughly arranged as follows. 
(Mermaid needed to visualize the graph below, or open this .md file in [StackEdit](https://stackedit.io))  

![](/home/stan/welding_setup_ws/src/welding_setup_packages/overview.png) 

```mermaid 
graph TB
classDef scriptfile fill:#f9f,stroke:#333,stroke-width:2px;
classDef urdffile fill:#3a9aba,stroke:#333,stroke-width:2px;

workpiece_py>scripts/workpiece_setup.py] --create urdf for workpiece --> workpiece_urdf[scripts/workpiece.urdf.xacro] ;

workpiece_urdf --included in--> master_urdf[ur5_robot_table.urdf.xacro];

master_urdf --urdf --> master((scripts/welding_setup.launch)) ;

audio(audio_capture/launch/capture.launch) --  link to audio capture script --> master; 

pipeline(ur_linmove/launch/moveit_planning_execution.launch)-- Pipeline for linear motion  --> master


master -- vizualization-->  rviz ;
master -- move group that controls robot --> move_group[/move_group]
class workpiece_py scriptfile;
master -- manual control of joints via sliders --> joint_state_publisher ;
 joint_state_publisher -. controls .-> move_group
master -- microphone audio topic  --> /welding_sound/audio
master --  tcp in the workpiece CS topic --> /tcp_weld_position

rviz -.visualizes/controls.-> move_group

master_py>scripts/welding_setup_master.py]-. controls .-> move_group
master_py --> bagfile
/welding_sound/audio -. records.-> master_py
/tcp_weld_position -. records.-> master_py
```	
--------------------

* First the URDF of the workpiece is created/updated by manually  running `scripts/workpiece_setup.py` 
* After that the main launch file `scripts/welding_setup.launch`  takes launches the move group for controlling the robot,  the topic for collecting audio audio and the current position of the  Weld gun in the workpieces CS.  

* The robot can then be controlled in 3 ways:  By dragging it around manually in rviz, by using the sliders on the joint_state_publisher, or by controlling it via the main Python script `scripts/welding_setup_master.py` .
* If the Python script is used,  the tcp can controlled precisely by specifying  a given speed, torch angle and weld length.  While the robot welds, the `/tcp_weld_position` and the `/welding_sound/audio` topics are recorded and saved in a bag. file 
* The bag file can then later be converted to a .wav file or csv file using the two utility scripts,  by running:
	 `$ rosrun audio_convert bag2wav --input=mybag.bag --output=audio.wav --input-audio-topic=/welding_sound/audio` (convert bag to wav) 
or copy the script `bag_to_csv.py` to the directory where the bag file is and execute:  
`$ python scripts bag_to_csv.py`  to automatically convert all bags in directory to .csv files.  

## How to use:
### Setting up:

* We assume you already have a working connection with the real UR5 (setup guide [here](https://github.com/UniversalRobots/Universal_Robots_ROS_Driver)) and the TCP properly set up to the tip of the welding torch via the FlexPad.  
* Using the FlexPad navigate the UR5 manually so the TCP touches the to the start and endpoint of the weld (orientation arbitrary),  write the xyz coordinates (relative to robot base) down. 
* Open the script `scripts/workpiece_setup.py`  in a text editor. Edit the variables `weld_start_point` and `weld_end_point` to reflect your real life setup.  Choose what height and width you would like for the visualized workpiece in rviz, by editing the `workpiece_width` & `workpiece_height` variables.

* Save and run the script:  `$ rosrun scripts workpiece_setup.py`.  It will generate an updated URDF `workpiece.urdf.xacro` for the workpiece.  You can check it is up to date, by opening it at checking the timestamp.

* Launch the main launch script:  `$ roslaunch scripts welding_setup.launch`. Rviz should pop open. 
* For ease of visualization we recommend  enabling the tool0 coordinate axis in rviz: In the 'Displays' menu select from the drop down tree: 'Scene Robot' -> 'Links' -> 'tool0' . Check the box 'Show Axes.'  Do likewise for the 'workpiece' link.  You should see something similar to the image below:  

![](screen1.png) 

### Moving the robot

* In your favorite Python IDE open  `scripts/welding_setup_master.py` Set the default `__ROBOT_VELOCITY__` to a safe level and set all other welding parameters to what you would like to have.  
* The `weld_start_point` is the starting point of the weld _in the workpiece CS_ ie. (0,0,0) coresponds to the corner of the visualized workpiece.  As default only the startpoint and a length is defined, and the weld is assumed to go along the x-axis, however a custom weld end point can be set as well. 
* `torch_long_angle` and `torch_trans_angle` are defined as follows:  
`torch_long_angle` :   Angle between vertical and the torch in the weld direction. An angle greater than zero means the torch is leaning backward in the direction of travel. 
`torch_trans_angle` :   Angle between vertical and the torch transverse to the weld direction.  An angle greater than zero means the torch is leaning to the right if looking along the weld axis.  __Please note: when using large angles on both torch trans angle and torch long angle on the same time, rounding errors can occur, and the tcp may miss the weld.__

![](torch_angles.png) 


## FAQ:
*  __Where do i change the setting for the audio topic? (node name, samplerate, bit depth etc.)__ :
The audio parameters are set in the master launch file `scripts/welding_setup.launch` and then passed on to the audio node.

* **Where do i change what 2 coordinate systems transformations the  tcp weld position is broadcasting?**
The audio tf transform  parameters are set in the master launch file `scripts/welding_setup.launch` and then passed on to the `tcp_weld_cs_broadcaster` node.

* **How do i get the linear motion planner to work fom my own robot URDF?**
Excellent question: For this purpose we have made an __entire step-by-step tutorial__ avaliable under:  `ur_linmove/how_to_setup.md`

* __If my robot has a different tool link how do i change the tool link name?__
As the default the setup uses  reference_frame=`world`, target_link = `tool0` when planning  To edit go to `pilz_robot_programming/commands.py` and edit the defaults. 


 
## Authors ##
Maciej Kolek - kolek.maciek@gmail.com
Stanislaw Zelazny  - stanislawzelazny@gmail.com
Aalborg University, spring 2020
## Revision of this document ##
__rev. 2.0__ - Last edited by Stanislaw Zelazny on 25.06.2020


