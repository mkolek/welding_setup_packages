#!/usr/bin/env python
from geometry_msgs.msg import Pose, Point
from pilz_robot_programming import *
#import pilz_robot_programming as pilz
import math
import rospy


#######
# import sys
import copy
# import moveit_commander
# import moveit_msgs.msg
# import actionlib
# import geometry_msgs
# import numpy as np
# from math import pi
# from std_msgs.msg import String
# from moveit_commander.conversions import pose_to_list
import time
####
import sys
import moveit_commander
import moveit_msgs.msg
import actionlib
import geometry_msgs

###

__REQUIRED_API_VERSION__ = "1"  # API version
__ROBOT_VELOCITY__ = 0.1        # velocity of the robot

# main program
def start_program():

    # define the positions:
    joint_goal = [0.0, -1.56, 1.56, -1.56, -1.56, 0.0]   # Use joint values for the first position
    cartesian_goal=Pose(position=Point(0.03, 0.46, 0.28), orientation=Quaternion(0,0.7,0,0.7) ) # Use cartesian coordinates for another position
    
    # Move to start point with joint values to avoid random trajectory
    r.move(Ptp(goal=joint_goal, vel_scale=__ROBOT_VELOCITY__, reference_frame="world", target_link = "ee_link"))
    
    rospy.sleep(1)
    # Move to the second position
    r.move(Ptp(goal=cartesian_goal, vel_scale=__ROBOT_VELOCITY__, reference_frame="world", target_link = "ee_link"))
    
    # move back, this time linear,
    r.move(Lin(goal=joint_goal, reference_frame="world", target_link = "ee_link", vel_scale=__ROBOT_VELOCITY__))


def linspeed_test():
  linear_velocity =  0.1
  y_distance = 1.0

  joint_start = [-0.97, -1.12, 1.35, -1.80, -1.57, 0.89]
  

  Point1=Pose(position=Point(0.5, -0.5, 0.3), orientation=Quaternion(0,0.7,0,0.7) )
  
  Point2 = copy.deepcopy(Point1)
  Point2.position.y =  Point2.position.y+y_distance

  while(True):

    print("Going to Point1")

    #r.move(Lin(goal = joint_start, reference_frame="world", target_link = "ee_link", vel_scale=__ROBOT_VELOCITY__))
            
    r.move(Ptp(goal=Point1, vel_scale=__ROBOT_VELOCITY__, reference_frame="world", target_link = "ee_link"))
    rospy.sleep(1)
    print("Starting move")
    starttime = time.time()
    r.move(Lin(goal=Point2, reference_frame="world", target_link = "ee_link", vel_scale=linear_velocity, acc_scale= 0.5))
    endtime = time.time()
    
    elapsedtime = endtime-starttime
    expectedtime = y_distance/linear_velocity
    print("=== move finished: ")
    print("=== time:"+ str(elapsedtime))
    print("=== expectd time:" +str(expectedtime))
    print("=== difference:"+ str(elapsedtime-expectedtime))
    rospy.sleep(1)
    print("repeating")
        




    
if __name__ == "__main__":
    # init a rosnode
    rospy.init_node('Pilz_move_node')

    # initialisation
    r = Robot(__REQUIRED_API_VERSION__)  # instance of the robot

    # start the main program
    # start_program()

    
    print( "============ Press `Enter` to use Pilz ...")
    raw_input()
    
    linspeed_test()

   


    print("finished with succes")

   
