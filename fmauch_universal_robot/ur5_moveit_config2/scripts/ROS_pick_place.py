#!/usr/bin/env python
# To use the python interface to move_group, import the moveit_commander
# module. We also import rospy and some messages that we will use.
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import actionlib
import geometry_msgs


def simple_pick_place():
  
  print( "============ Press `Enter` to start moving  ...")
  raw_input()
  ## First initialize moveit_commander and rospy.
  moveit_commander.roscpp_initialize(sys.argv)
  rospy.init_node('simple_pick_place',
                  anonymous=True)

  ## Instantiate a MoveGroupCommander object.  This object is an interface
  ## to one group of joints.  In this case the group refers to the joints of
  ## robot1. This interface can be used to plan and execute motions on robot1.
  ur5 = moveit_commander.MoveGroupCommander("manipulator")
  ## MoveGroup Commander Object for robot2.

  ## Action clients to the ExecuteTrajectory action server.
  ur5_client = actionlib.SimpleActionClient('execute_trajectory',
    moveit_msgs.msg.ExecuteTrajectoryAction)
  ur5_client.wait_for_server()
  rospy.loginfo('Execute Trajectory server is available for ur5')
 
  ## Set a named joint configuration as the goal to plan for a move group.
  ## Named joint configurations are the robot poses defined via MoveIt! Setup Assistant.
  ur5.set_named_target("home")

  ## Plan to the desired joint-space goal using the default planner (RRTConnect).
  ur5_plan_home = ur5.plan()
  ## Create a goal message object for the action server.
  ur5_goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
  ## Update the trajectory in the goal message.
  ur5_goal.trajectory = ur5_plan_home

  ## Send the goal to the action server.

  ur5_client.send_goal(ur5_goal)
  ur5_client.wait_for_result()

  print("=Done")
  print( "============ Press `Enter` to start linear ...")
  raw_input()

  rospy.sleep(3)



  ## Cartesian Paths
  ## ^^^^^^^^^^^^^^^
  ## You can plan a cartesian path directly by specifying a list of waypoints
  ## for the end-effector to go through.
  waypoints = []

  distance = 1.0 

  start_pose = geometry_msgs.msg.Pose()
  start_pose.position.x = 0.5
  start_pose.position.y = -0.5
  start_pose.position.z = 0.3
  start_pose.orientation.x =  0.0
  start_pose.orientation.y = 0.7
  start_pose.orientation.z = 0.0
  start_pose.orientation.w = 0.7

  end_pose = copy.deepcopy(start_pose)

  end_pose.position.y +=  distance


  waypoints.append(start_pose)
  waypoints.append(end_pose)
  
  ## We want the cartesian path to be interpolated at a resolution of 1 cm
  ## which is why we will specify 0.01 as the eef_step in cartesian
  ## translation.  We will specify the jump threshold as 0.0, effectively
  ## disabling it.
  fraction = 0.0

  for count_cartesian_path in range(0,3):
    if fraction < 1.0:
      (plan_cartesian, fraction) = ur5.compute_cartesian_path(
                                   waypoints,   # waypoints to follow
                                   0.01,        # eef_step
                                   0.0)         # jump_threshold
    else:
      break


  print("moving in a loop")    
  while(True):

    print("Linear move")
    
    ur5_goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
    ur5_goal.trajectory = plan_cartesian
    ur5_client.send_goal(ur5_goal)
    ur5_client.wait_for_result()

    print("linear move finished")
    rospy.sleep(1)
    print("going back ")

    ur5.set_pose_target(start_pose)
    ur5.go(wait=True)
    print("back at at start")
    rospy.sleep(1)


  
  moveit_commander.roscpp_shutdown()


if __name__=='__main__':
  try:
    simple_pick_place()
  except rospy.ROSInterruptException:
    pass
