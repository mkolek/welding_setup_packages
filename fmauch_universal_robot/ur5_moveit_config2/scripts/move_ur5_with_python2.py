#!/usr/bin/env python
# To use the python interface to move_group, import the moveit_commander
# module. We also import rospy and some messages that we will use.
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import actionlib
import geometry_msgs
import numpy as np
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
import time


def close_enough(goal, actual, tolerance):
  """
  Convenience method for testing if a list of values are within a tolerance of their counterparts in another list
  @param: goal       A list of floats, a Pose or a PoseStamped
  @param: actual     A list of floats, a Pose or a PoseStamped
  @param: tolerance  A float
  @returns: bool
  """
  all_equal = True
  if type(goal) is list:
    for index in range(len(goal)):
      if abs(actual[index] - goal[index]) > tolerance:
        return False

  elif type(goal) is geometry_msgs.msg.PoseStamped:
    return close_enough(goal.pose, actual.pose, tolerance)

  elif type(goal) is geometry_msgs.msg.Pose:
    return close_enough(pose_to_list(goal), pose_to_list(actual), tolerance)

  return True


class MoveGroupPython(object):
 
  def __init__(self, nodename, group_name ):
   # super(MoveGroupPythonIntefaceTutorial, self).__init__()

    ## First initialize `moveit_commander`_ and a `rospy`_ node:
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node(str(nodename), anonymous=True)

    ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
    ## kinematic model and the robot's current joint states
    self.robot = moveit_commander.RobotCommander()

    ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
    ## for getting, setting, and updating the robot's internal understanding of the
    ## surrounding world:
    self.scene = moveit_commander.PlanningSceneInterface()

    ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
    ## to a planning group (group of joints). 
    ## If you are using a different robot, change this value to the name of your robot
    ## arm planning group.
    ## This interface can be used to plan and execute motions:
    
    self.move_group = moveit_commander.MoveGroupCommander(str(group_name))

    ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
    ## trajectories in Rviz:
    self.display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                   moveit_msgs.msg.DisplayTrajectory,
                                                   queue_size=20)


    ##  Printing Basic Information
    ## ^^^^^^^^^^^^^^^^^^^^^^^^^
    # We can get the name of the reference frame for this robot:
    print( "Robot created succesfully!")
    self.planning_frame = self.move_group.get_planning_frame()
    print( "============ Planning frame: %s" % self.planning_frame)

    # We can also print the name of the end-effector link for this group:
    self.eef_link = self.move_group.get_end_effector_link()
    print( "============ End effector link: %s" % self.eef_link)

    # We can get a list of all the groups in the robot:
    self.group_names = self.robot.get_group_names()
    print ("============ Available Planning Groups:", self.robot.get_group_names())

    # Sometimes for debugging it is useful to print the entire state of the
    # robot:
    print ("============ Printing robot state:")
    print (self.robot.get_current_state())
    print ("")
    print ("==============Ready to move!" )
    print ("")   

    # Object atributess
    # self.box_name = ''
    # self.robot = robot
    # self.scene = scene
    # self.move_group = move_group
    # self.display_trajectory_publisher = display_trajectory_publisher
    # self.planning_frame = planning_frame
    # self.eef_link = eef_link
    # self.group_names = group_names





  def go_to_joint_state(self, joint_target = np.array([0.0,-pi/2, pi/2, -pi/2,-pi/2,0.0])):
   
    print("====== moving to joint state..")
    # if len(joint_target) != 6:
    #   raise ValueError print( "input joint target is supposed to be 1x6 vector!")


    joint_goal = self.move_group.get_current_joint_values() # Get a valid joint goal format..
    joint_goal[0] = joint_target[0]  # Fill in with own values..
    joint_goal[1] = joint_target[1]
    joint_goal[2] = joint_target[2]
    joint_goal[3] = joint_target[3]
    joint_goal[4] = joint_target[4]
    joint_goal[5] = joint_target[5]

    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    self.move_group.go(joint_goal, wait=True)

    # Calling ``stop()`` ensures that there is no residual movement
    self.move_group.stop()

    # For testing:
    current_joints = self.move_group.get_current_joint_values()
    if (close_enough(joint_goal, current_joints, 0.01)):
      print("====joint state archeived! :")
      print(current_joints)
    return


  def get_pose(self, printit = True):
    pose = self.move_group.get_current_pose()

    if(printit):
      print("=== The robots pose is:")
      print(pose)
      print("====================")
    return pose


  def go_to_pose_goal(self, position , orientation = None ):    # Not tested!!
    ''' NOTE: Requires end effector !
    position :  [x,y,z] (m)
    orientation:  [x,y,z,w] (quaterion notation)
    if no orientation is given, the current orientation of the EEF is used.
    '''


    if len(str(self.eef_link)) == 0:
      print(" ==== got_to_pose_goal: -- Error! No end effector mounted!")
      return

    pose_goal = geometry_msgs.msg.Pose()

    if orientation is None:  #If no orientation given, keep current orientation. 
      pose_goal.orientation = self.move_group.get_current_pose().pose.orientation
    else:
      pose_goal.orientation.x = orientation[0]  # Else use the input 
      pose_goal.orientation.y = orientation[1]
      pose_goal.orientation.z = orientation[2]
      pose_goal.orientation.w = orientation[3]

    pose_goal.position.x = position[0]
    pose_goal.position.y = position[1]
    pose_goal.position.z = position[2]

    self.move_group.set_pose_target(pose_goal)

    ## Now, we call the planner to compute the plan and execute it.
    print("=== going to pose goal:")
    print("position: ["+ str(pose_goal.position.x) +"," + str(pose_goal.position.y) +"," + str(pose_goal.position.z) +"]")
    print("orientation: ["+ str(pose_goal.orientation.x) + "," + str(pose_goal.orientation.y) +"," + str(pose_goal.orientation.z)  +"," + str(pose_goal.orientation.w) + "]")


    self.move_group.go(wait=True)
    # Calling `stop()` ensures that there is no residual movement
    self.move_group.stop()   
     # It is always good to clear your targets after planning with poses.
    self.move_group.clear_pose_targets()

    current_pose = self.move_group.get_current_pose().pose
    if(  close_enough(pose_goal, current_pose, 0.01)):
      print("===pose goal acheived!")
    return

  #def weld_path()

  def plan_cartesian_path(self, scale=1):


    ## You can plan a Cartesian path directly by specifying a list of waypoints
    ## for the end-effector to go through. If executing  interactively in a
    ## Python shell, set scale = 1.0.
    ##
    waypoints = []

    wpose = self.move_group.get_current_pose().pose
    
    wpose.position.z -= scale * 0.1  # First move up (z)
    wpose.position.y += scale * 0.2  # and sideways (y)
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.x += scale * 0.1  # Second move forward/backwards in (x)
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.y -= scale * 0.1  # Third move sideways (y)
    waypoints.append(copy.deepcopy(wpose))

    # We want the Cartesian path to be interpolated at a resolution of 1 cm
    # which is why we will specify 0.01 as the eef_step in Cartesian
    # translation.  We will disable the jump threshold by setting it to 0.0,
    # ignoring the check for infeasible jumps in joint space, which is sufficient
    # for this tutorial.
    (plan, fraction) = self.move_group.compute_cartesian_path(
                                       waypoints,   # waypoints to follow
                                       0.01,        # eef_step
                                       0.0)         # jump_threshold

    # Note: We are just planning, not asking move_group to actually move the robot yet:
    return plan, fraction

    ## END_SUB_TUTORIAL



  def execute_plan(self, plan):
    # Copy class variables to local variables to make the web tutorials more clear.
    # In practice, you should use the class variables directly unless you have a good
    # reason not to.
    move_group = self.move_group

    ## BEGIN_SUB_TUTORIAL execute_plan
    ##
    ## Executing a Plan
    ## ^^^^^^^^^^^^^^^^
    ## Use execute if you would like the robot to follow
    ## the plan that has already been computed:
    move_group.execute(plan, wait=True)

    ## **Note:** The robot's current joint state must be within some tolerance of the
    ## first waypoint in the `RobotTrajectory`_ or ``execute()`` will fail
    ## END_SUB_TUTORIAL


#   def wait_for_state_update(self, box_is_known=False, box_is_attached=False, timeout=4):
#     # Copy class variables to local variables to make the web tutorials more clear.
#     # In practice, you should use the class variables directly unless you have a good
#     # reason not to.
#     box_name = self.box_name
#     scene = self.scene

#     ## BEGIN_SUB_TUTORIAL wait_for_scene_update
#     ##
#     ## Ensuring Collision Updates Are Receieved
#     ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#     ## If the Python node dies before publishing a collision object update message, the message
#     ## could get lost and the box will not appear. To ensure that the updates are
#     ## made, we wait until we see the changes reflected in the
#     ## ``get_attached_objects()`` and ``get_known_object_names()`` lists.
#     ## For the purpose of this tutorial, we call this function after adding,
#     ## removing, attaching or detaching an object in the planning scene. We then wait
#     ## until the updates have been made or ``timeout`` seconds have passed
#     start = rospy.get_time()
#     seconds = rospy.get_time()
#     while (seconds - start < timeout) and not rospy.is_shutdown():
#       # Test if the box is in attached objects
#       attached_objects = scene.get_attached_objects([box_name])
#       is_attached = len(attached_objects.keys()) > 0

#       # Test if the box is in the scene.
#       # Note that attaching the box will remove it from known_objects
#       is_known = box_name in scene.get_known_object_names()

#       # Test if we are in the expected state
#       if (box_is_attached == is_attached) and (box_is_known == is_known):
#         return True

#       # Sleep so that we give other threads time on the processor
#       rospy.sleep(0.1)
#       seconds = rospy.get_time()

#     # If we exited the while loop without returning then we timed out
#     return False
#     ## END_SUB_TUTORIAL


#   def add_box(self, timeout=4):
#     # Copy class variables to local variables to make the web tutorials more clear.
#     # In practice, you should use the class variables directly unless you have a good
#     # reason not to.
#     box_name = self.box_name
#     scene = self.scene

#     ## BEGIN_SUB_TUTORIAL add_box
#     ##
#     ## Adding Objects to the Planning Scene
#     ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#     ## First, we will create a box in the planning scene at the location of the left finger:
#     box_pose = geometry_msgs.msg.PoseStamped()
#     box_pose.header.frame_id = "panda_leftfinger"
#     box_pose.pose.orientation.w = 1.0
#     box_pose.pose.position.z = 0.07 # slightly above the end effector
#     box_name = "box"
#     scene.add_box(box_name, box_pose, size=(0.1, 0.1, 0.1))

#     ## END_SUB_TUTORIAL
#     # Copy local variables back to class variables. In practice, you should use the class
#     # variables directly unless you have a good reason not to.
#     return self.wait_for_state_update(box_is_known=True, timeout=timeout)


#   def attach_box(self, timeout=4):
#     # Copy class variables to local variables to make the web tutorials more clear.
#     # In practice, you should use the class variables directly unless you have a good
#     # reason not to.
#     box_name = self.box_name
#     robot = self.robot
#     scene = self.scene
#     eef_link = self.eef_link
#     group_names = self.group_names

#     ## BEGIN_SUB_TUTORIAL attach_object
#     ##
#     ## Attaching Objects to the Robot
#     ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#     ## Next, we will attach the box to the Panda wrist. Manipulating objects requires the
#     ## robot be able to touch them without the planning scene reporting the contact as a
#     ## collision. By adding link names to the ``touch_links`` array, we are telling the
#     ## planning scene to ignore collisions between those links and the box. For the Panda
#     ## robot, we set ``grasping_group = 'hand'``. If you are using a different robot,
#     ## you should change this value to the name of your end effector group name.
#     grasping_group = 'hand'
#     touch_links = robot.get_link_names(group=grasping_group)
#     scene.attach_box(eef_link, box_name, touch_links=touch_links)
#     ## END_SUB_TUTORIAL

#     # We wait for the planning scene to update.
#     return self.wait_for_state_update(box_is_attached=True, box_is_known=False, timeout=timeout)


#   def detach_box(self, timeout=4):
#     # Copy class variables to local variables to make the web tutorials more clear.
#     # In practice, you should use the class variables directly unless you have a good
#     # reason not to.
#     box_name = self.box_name
#     scene = self.scene
#     eef_link = self.eef_link

#     ## BEGIN_SUB_TUTORIAL detach_object
#     ##
#     ## Detaching Objects from the Robot
#     ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#     ## We can also detach and remove the object from the planning scene:
#     scene.remove_attached_object(eef_link, name=box_name)
#     ## END_SUB_TUTORIAL

#     # We wait for the planning scene to update.
#     return self.wait_for_state_update(box_is_known=True, box_is_attached=False, timeout=timeout)


#   def remove_box(self, timeout=4):
#     # Copy class variables to local variables to make the web tutorials more clear.
#     # In practice, you should use the class variables directly unless you have a good
#     # reason not to.
#     box_name = self.box_name
#     scene = self.scene

#     ## BEGIN_SUB_TUTORIAL remove_object
#     ##
#     ## Removing Objects from the Planning Scene
#     ## ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#     ## We can remove the box from the world.
#     scene.remove_world_object(box_name)

#     ## **Note:** The object must be detached before we can remove it from the world
#     ## END_SUB_TUTORIAL

#     # We wait for the planning scene to update.
#     return self.wait_for_state_update(box_is_attached=False, box_is_known=False, timeout=timeout)




# def simple_pick_place():

#   print "============ Press `Enter` to execute a movement using a joint state goal ..."
#   raw_input()
#   ## First initialize moveit_commander and rospy.
#   moveit_commander.roscpp_initialize(sys.argv)
#   rospy.init_node('move_ur5_with_python',
#                   anonymous=True)

#   ## Instantiate a MoveGroupCommander object.  This object is an interface
#   ## to one group of joints.  In this case the group refers to the joints of
#   ## robot1. This interface can be used to plan and execute motions on robot1.
#   robot1_group = moveit_commander.MoveGroupCommander("ur5_group")

#   ## Action clients to the ExecuteTrajectory action server.
#   robot1_client = actionlib.SimpleActionClient('execute_trajectory',
#     moveit_msgs.msg.ExecuteTrajectoryAction)
#   robot1_client.wait_for_server()
#   rospy.loginfo('Execute Trajectory server is available for ur5_group')

#   ## Set a named joint configuration as the goal to plan for a move group.
#   ## Named joint configurations are the robot poses defined via MoveIt! Setup Assistant.
#   robot1_group.set_named_target("home_90deg")

#   ## Plan to the desired joint-space goal using the default planner (RRTConnect).
#   robot1_plan_home = robot1_group.plan()
#   ## Create a goal message object for the action server.
#   robot1_goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
#   ## Update the trajectory in the goal message.
#   robot1_goal.trajectory = robot1_plan_home

#   ## Send the goal to the action server.
#   robot1_client.send_goal(robot1_goal)
#   robot1_client.wait_for_result()
# # 
# if False:
# 	  robot1_group.set_named_target("R1PreGrasp")
# 	  robot1_plan_pregrasp = robot1_group.plan()
# 	  robot1_goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
# 	  robot1_goal.trajectory = robot1_plan_pregrasp
# 	  robot1_client.send_goal(robot1_goal)
# 	  robot1_client.wait_for_result()

# 	  ## Cartesian Paths
# 	  ## ^^^^^^^^^^^^^^^
# 	  ## You can plan a cartesian path directly by specifying a list of waypoints
# 	  ## for the end-effector to go through.
# 	  waypoints = []
# 	  # start with the current pose
# 	  current_pose = robot1_group.get_current_pose()
# 	  rospy.sleep(0.5)
# 	  current_pose = robot1_group.get_current_pose()

# 	  ## create linear offsets to the current pose
# 	  new_eef_pose = geometry_msgs.msg.Pose()

# 	  # Manual offsets because we don't have a camera to detect objects yet.
# 	  new_eef_pose.position.x = current_pose.pose.position.x + 0.10
# 	  new_eef_pose.position.y = current_pose.pose.position.y - 0.20
# 	  new_eef_pose.position.z = current_pose.pose.position.z - 0.20

# 	  # Retain orientation of the current pose.
# 	  new_eef_pose.orientation = copy.deepcopy(current_pose.pose.orientation)

# 	  waypoints.append(new_eef_pose)
# 	  waypoints.append(current_pose.pose)
# 	  print(new_eef_pose.position)
# 	  print(current_pose.pose.position)

# 	  ## We want the cartesian path to be interpolated at a resolution of 1 cm
# 	  ## which is why we will specify 0.01 as the eef_step in cartesian
# 	  ## translation.  We will specify the jump threshold as 0.0, effectively
# 	  ## disabling it.
# 	  fraction = 0.0
# 	  for count_cartesian_path in range(0,3):
# 	    if fraction < 1.0:
# 	      (plan_cartesian, fraction) = robot1_group.compute_cartesian_path(
# 		                           waypoints,   # waypoints to follow
# 		                           0.01,        # eef_step
# 		                           0.0)         # jump_threshold
# 	    else:
# 	      break

# 	  robot1_goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
# 	  robot1_goal.trajectory = plan_cartesian
# 	  robot1_client.send_goal(robot1_goal)
# 	  robot1_client.wait_for_result()

# 	  robot1_group.set_named_target("R1Place")
# 	  robot1_plan_place = robot1_group.plan()
# 	  robot1_goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
# 	  robot1_goal.trajectory = robot1_plan_place

# 	  robot1_client.send_goal(robot1_goal)
# 	  robot1_client.wait_for_result()
# 	  ## When finished shut down moveit_commander.
# 	  moveit_commander.roscpp_shutdown()


# if __name__=='__main__':
#   try:
#     simple_pick_place()
#   except rospy.ROSInterruptException:
#     pass


def main():
  
    # print( "============ Press `Enter` to begin setting up the moveit_commander ...")
    # raw_input()

    ur5 =  MoveGroupPython("move_ur5_with_python2","manipulator")
    ur5.go_to_joint_state()

    # print( "============ Press `Enter` ...")
    # raw_input()

    # current_pose = ur5.move_group.get_current_pose().pose
    # print(current_pose)
    ur5.get_pose() 

    plan, fraction = ur5.plan_cartesian_path()
    print(plan)



    print( "============ Press `Enter` ...")
    raw_input()
    time.sleep(3)
    ur5.go_to_pose_goal([0.5,0.2,0.4])


  

  

if __name__=='__main__':
  try:
    main()
  except rospy.ROSInterruptException:
    pass
