#!/usr/bin/env python  
import rospy

import math
import tf2_ros
import tf2_msgs.msg
import geometry_msgs.msg
import turtlesim.srv

if __name__ == '__main__':
    
    rospy.init_node('tcp_weld_broadcaster')

    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    # create publisher to publish the relative position
    publisher = rospy.Publisher('tcp_weld_position', geometry_msgs.msg.Point, queue_size=1)

    # Get the name of the two frames to be compared from the args in the launch file:
    tcp_frame_name = rospy.get_param('tcp_frame', 'tool0') # 'tool0 is default'
    reference_frame_name =  rospy.get_param('reference_frame','workpiece') # 'workpiece' is default 
    broadcast_rate =  rospy.get_param('broadcast_rate',10.0)


    rate = rospy.Rate(broadcast_rate)
    while not rospy.is_shutdown():
        try:
            relative_pos = tfBuffer.lookup_transform(reference_frame_name, tcp_frame_name, rospy.Time()) # get transformation between the two frames. 
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            rospy.loginfo('Trying to connnect to tf transforms:' + tcp_frame_name + ' & ' + reference_frame_name+ ', re-trying...')
            rate.sleep()
           
            continue
        
        rospy.loginfo_once('TF connection succesful! ')
        
        translation  = relative_pos.transform.translation
        message = geometry_msgs.msg.Point(translation.x , translation.y, translation.z)
        publisher.publish(message)

        rate.sleep()
