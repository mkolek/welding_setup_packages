import numpy as np
import tf.transformations
import os
from datetime import datetime
now = datetime.now()


''' This scrit is for creating the launch file that is responsible for setting up the workpiece in RVIZ.  
The XYZ positions (in the robot base coordinate system) of the start and end of the weld are input. 
The script automatticaly calculates the rotation and length of the weld, and creates the weld coordinate system.  
It creates a grey box, representing the weld and inputs it into RVIZ . '''



#### UTILITY FUNCTIONS #######

def extend_4x4(rotation_matrix_3x3):
  ''' extends a 3x3 rotation matrix onto a 4x4 transformation matrix (no translation, just padding) '''
  
  r_4x4 = [[rotation_matrix_3x3[0, 0], rotation_matrix_3x3[0, 1], rotation_matrix_3x3[0, 2], 0],
           [rotation_matrix_3x3[1, 0], rotation_matrix_3x3[1, 1], rotation_matrix_3x3[1, 2], 0],
           [rotation_matrix_3x3[2, 0], rotation_matrix_3x3[2, 1], rotation_matrix_3x3[2, 2], 0],
           [0, 0, 0, 1]]
  return r_4x4

def rotation_matrix_for_unit_vectors (unit_v1, unit_v2):
    ''' The function gives a matrix that  rotates the unit vector v1 onto v2 '''

    # Find rotation Matrix R
    #alpha = angle_between_vectors(unit_v1, unit_v2)
    v = np.cross(unit_v1, unit_v2)
    s = np.linalg.norm(v) #* np.sin(alpha)
    c = np.dot(unit_v1, unit_v2) #* np.cos(alpha)

    vx = np.asarray([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    #last_formula = 1/(1+c)
    last_formula = 1/(1+c)
    i = np.identity(3)

    r = i + vx + np.dot(vx, vx) * last_formula #    dot(vx,vx) !!!!!!!!
    return extend_4x4(r)


########### INPUT DATA HERE  #########

weld_start_point =  np.array([0.3, -0.25, 0.0])  #xyz position of the weld start point in the robot base cs system
weld_end_point = np.array([0.3, 0.25, 0.0])

workpiece_width= 0.1    # width and height of the box that is drawn in rviz [meters]
workpiece_height = 0.01  
 
filename = "workpiece.urdf.xacro"  # filename tp save to

############# CALCULATE ################

weld_vector =  weld_end_point-weld_start_point
weld_length = np.linalg.norm(weld_vector)
weld_unit = weld_vector/weld_length

# (the box's origin is its ceentroid)
box_origin = [weld_length/2 , -workpiece_width/2, -workpiece_height/2]  # The box origin is always in the joints CS
box_origin_str =str(box_origin[0])+' '+str(box_origin[1])+' '+str(box_origin[2])

box_size_str = str(weld_length)+' '+str(workpiece_width)+' '+str(workpiece_height)

rot_mat = rotation_matrix_for_unit_vectors([1,0,0], weld_unit)
weld_rpy = tf.transformations.euler_from_matrix(rot_mat, 'sxyz')
weld_rpy_str = str(weld_rpy[0])+' '+str(weld_rpy[1])+' '+str(weld_rpy[2])

weld_cs_xyz_str = str(weld_start_point[0]) +' '+str(weld_start_point[1])+' '+str(weld_start_point[2])


#####  CREATE FILE ###########

if False: # temp disable 
  if os.path.exists(filename):
          print("File: '" + filename +" 'exists, overwrite? (Y/N)")
          rep= raw_input()
          if rep == "Y" or  rep == "y":
              print('overwriting...')
              os.remove(filename)
          elif rep == "N" or rep == "n":
              filename = "workpiece_"+ str(now.day)+str(now.month)+str(now.year)+"_"+str(now.hour)+str(now.minute)+".unnrdf.xacro"
              print('saving as: ' + filename + ' instead')
          else:
              print("invalid answer, exiting program!")
              exit(1)


#### CREATING THE LAUNCH FILE #############

file = open(filename, "w")

# header
file.write('<?xml version="1.0"?> \n')
file.write('<robot xmlns:xacro="http://wiki.ros.org/xacro"> \n \n')

file.write("<!-- workpiece -->\n")
file.write("<!-- Date created:" + str(now.ctime())+  " --> \n")
file.write('<!-- the  welding edge of the workpiece is closer to the robot base -->\n \n')

#custom colors
file.write(
    '<!-- coloring of the workpiece -->\n'
    ' <material name="grey"> \n'
    '   <color rgba="0.75 0.75 0.75 1"/>\n'
    ' </material>\n'
)

#workpiece  
file.write(
   '<link name="workpiece"> \n'
   ' <visual>\n'
   '   <origin rpy="0.0 0.0 0.0" xyz="'+ box_origin_str +'"/>   <!-- origin and rot in the workpiece_joint CS --> \n'
   '      <geometry>\n'
   '          <box size="'+ box_size_str +'"/>\n'
   '      </geometry>\n'
   '    <material name="grey"/>\n'
   ' </visual>\n'
   '</link>\n \n '
)

#joint 
file.write(
'<joint name="workpiece_joint" type="fixed">\n'
'    <parent link="world" />\n'
'    <child link = "workpiece" />\n'
'    <origin xyz= "'+ weld_cs_xyz_str+ '" rpy="'+weld_rpy_str+'" />\n '
'</joint>\n \n'
)

file.write('</robot>')

file.close()


print('program done!')



