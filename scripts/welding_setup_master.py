#!/usr/bin/env python
from geometry_msgs.msg import Pose, Point
from std_msgs.msg import Bool, UInt16
from pilz_robot_programming import *
import math
import rospy
import copy
import time
import numpy as np
from math import pi
from tf.transformations import *
from multiprocessing import Process
import subprocess, shlex

__REQUIRED_API_VERSION__ = "1"  # API version
__ROBOT_VELOCITY__ = 0.5        #  Default velocity of the robot.  1.0 -> 1 m/s 




###  GENERAL: ###########

#Defaults: reference_frame="world", target_link = "tool0" .
# To edit go to pilz_robot_programming/commands.py


### WELDING ###############

def linear_welding(): 

  ### INPUT ######## 


  home_pose_joints = [0.0, -1.56, 1.56, -1.56, -1.56, 0.0]  # Joints of the home pose. 


  welding_speed = 0.1  # Speed during the weld.  1.0 = 1 m/s
  electrode_offset_mm = 50*0.001 # Electrode offset in mm
  
  torch_long_angle = 0.0  #Torch angle in the weld direction.  = 0 --> vertical.  x>0 --> __\ -> .   x<0 --> __/ ->    (in degrees)
  torch_transverse_angle = 0.0 # Torch angle transverse to weld direction.  x>0 ---> leaning right in dir of travel. 
  
  weld_start_point = np.array([0.0,0.0,0.0])  # XYZ Point on the workpiece CS where the weld should start.
                              # If you are not welding along the CS x-axis, chage, otherwise keep default a
  weld_length = 0.5
  
  weld_end_point =  None # If you are not welding along the CS x-axis, chage, otherwise keep default 

  ### CALCULATE POSITIONS #######

  if weld_end_point is None:
    weld_end_point = copy.deepcopy(weld_start_point)
    weld_end_point[0] = weld_end_point[0] + weld_length

  # offset of TCP due to torch angle and electrode offset

  unitv = np.array([0.0,0.0,1.0])
  Re = euler_matrix(math.radians(-torch_transverse_angle),math.radians(torch_long_angle),0,'sxyz')
  tcp_offset = unitv.dot(Re[0:3,0:3])*electrode_offset_mm
  
  
  weld_start_point = weld_start_point+tcp_offset
  weld_end_point = weld_end_point +tcp_offset

  # Torch orientation due to torch angle  

  q_base_orient =  quaternion_about_axis(math.radians(180), (1,0,0)) # basic qaternion for torch position relatice to workpiece cs
  q_torch_long = quaternion_about_axis(math.radians(torch_long_angle), (0,1,0))  # quat for torch long angle
  q_torch_trans = quaternion_about_axis(math.radians(torch_transverse_angle),(1,0,0)) # quat for torch trans angle
  torch_orient = quaternion_multiply(q_base_orient, q_torch_long)
  torch_orient = quaternion_multiply(torch_orient, q_torch_trans)
  
  # Calculate weld poses:
  weld_start_pose = Pose(position= Point(weld_start_point[0], weld_start_point[1],weld_start_point[2] ), orientation =  Quaternion(torch_orient[0],torch_orient[1],torch_orient[2],torch_orient[3]))
  weld_end_pose = Pose(position= Point(weld_end_point[0],weld_end_point[1],weld_end_point[2]), orientation =  Quaternion(torch_orient[0],torch_orient[1],torch_orient[2],torch_orient[3]))

  ### MOVEMENT ######

  ask_before_move = True  #Prompt user before each movement.  Can be set to False to ease debugging

  #### Publishers #####
  weldgun = rospy.Publisher('toggleKlixon', Bool, queue_size=10)
  #rospy.init_node('weldgun_cmd', anonymous=True)


  # Move to home position: 
  if ask_before_move:
    print("=== Press 'Enter' to move  to home position")
    raw_input()

  r.move(Ptp(goal=home_pose_joints, vel_scale=__ROBOT_VELOCITY__ ))
  print("=== At home position.")
  rospy.sleep(1)

  if ask_before_move:
    print("=== Press 'Enter' to move  to weld startpoint")
    raw_input()

  r.move(Ptp(goal=weld_start_pose, vel_scale=__ROBOT_VELOCITY__, reference_frame = "workpiece" ))
  print("=== At weld startpoint.")

 
  if ask_before_move:
    print("=== Press 'Enter' to start welding ....")
    raw_input()

  # Welding here -----------------------------------------------------------
  print("=== Welding ...")

  # Start recording rosbag for two topics: /welding_sound/audio and /tcp_weld_position

  start_recording = "rosbag record -o weld_bag /welding_sound/audio /tcp_weld_position /potValue __name:=my_bag"
  start_recording = shlex.split(start_recording)
  subprocess.Popen(start_recording)

  #Start welding gun:
  weldgun_command = True
  weldgun.publish(weldgun_command)

  # Move robot:
  r.move(Lin(goal=weld_end_pose, vel_scale=welding_speed, acc_scale = 1, reference_frame = "workpiece" ))

  # Stop weldign gun:

  weldgun_command = False
  weldgun.publish(weldgun_command)

  
  # Stop recording rosbag for two topics: /welding_sound/audio and /tcp_weld_position
  stop_recording = "rosnode kill /my_bag"
  stop_recording = shlex.split(stop_recording)
  rosbag_proc_stop = subprocess.Popen(stop_recording)

  print("=== weld ended (and rosbag saved).")
  rospy.sleep(1)


  # ---------------------------------

  if ask_before_move:
    print("=== Press 'Enter' to move  to home position")
    raw_input()

  r.move(Ptp(goal=home_pose_joints, vel_scale=__ROBOT_VELOCITY__ ))
  print("=== At home position.")




    
if __name__ == "__main__":
    # init a rosnode
    rospy.init_node('welding_robot_control')

    # initialisation
    r = Robot(__REQUIRED_API_VERSION__)  # instance of the robot

    linear_welding()

    print("finished with succes")

   
