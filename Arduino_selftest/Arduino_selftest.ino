
// This script is used to test the arduino breadboard for loose connnections etc.


// Pins:
const int klixonPin = 2;
const int blueLedPin = 5;
const int buzzerPin = 6;
const int potPin = A5;

//Globals:
unsigned long oldTimeKlixon = 0;
long klixonInterval = 2000;
bool klixonState = false;



void setup() {


  Serial.begin(9600);
  Serial.println("Ready..");

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(klixonPin, OUTPUT);
  pinMode(blueLedPin, OUTPUT);
  pinMode(buzzerPin, OUTPUT);

  pinMode(potPin, INPUT);


}

void loop() {

  unsigned long currTime = millis();

  if (currTime - oldTimeKlixon >= klixonInterval ) {  // flip relay every 2 sek.
    oldTimeKlixon = currTime;


    if (klixonState == false) {
      digitalWrite(klixonPin, HIGH);
      digitalWrite(LED_BUILTIN, HIGH);
      klixonState = true;
    }

    else {
      digitalWrite(klixonPin, LOW);
      digitalWrite(LED_BUILTIN, LOW);
      klixonState = false;
    }

  }


    // Read potentiometer
    int potread = 1024 - analogRead(potPin);
    int ledLevel = map(potread, 0, 1025, 0, 255); // map pot reading to blue LED.
    analogWrite(blueLedPin, ledLevel);

    tone(buzzerPin, map(potread, 0, 1025, 50, 1500)); // map pot reading to tone.

    Serial.print(potread); Serial.print("\t"); Serial.print(ledLevel); Serial.print("\t"); Serial.print(klixonState); Serial.print("\n");
    delay(10);


}
