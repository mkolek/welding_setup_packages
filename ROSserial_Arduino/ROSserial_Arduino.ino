
// Arduino ROS Serial demonstrator.

#include <ros.h>
#include <std_msgs/Bool.h>  // boolean message for toggling the klixon (the relay)
#include <std_msgs/UInt16.h>  // Unsigned 16 bit for publishing the potentiometer value


// Pins:
const int klixonPin = 2;
const int blueLedPin = 5;
const int buzzerPin = 6;
const int potPin = A5;


ros::NodeHandle nh; // create node handler



void klixonCb(const std_msgs::Bool& klixonCmd) {
  if (klixonCmd.data == true) {
    digitalWrite(klixonPin, HIGH);
    nh.loginfo("Arduino: Klixon On!");
  }

  if (klixonCmd.data == false) {
    digitalWrite(klixonPin, LOW);
    nh.loginfo("Arduino: Klixon off");
  }
}


// Create objects
ros::Subscriber<std_msgs::Bool> klixonSub("toggleKlixon", &klixonCb);

std_msgs::UInt16 potVal;
ros::Publisher potPub("potValue", &potVal );


void setup() {

  // Init pins:
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(blueLedPin, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  pinMode(potPin, INPUT);



  nh.initNode();
  nh.advertise(potPub);
  nh.subscribe(klixonSub);


}

void loop() {


  // Read potentiometer
  int potread = 1024 - analogRead(potPin);
  int ledLevel = map(potread, 0, 1025, 0, 255); // map pot reading to blue LED.
  analogWrite(blueLedPin, ledLevel);

  tone(buzzerPin, map(potread, 0, 1025, 50, 1500)); // map pot reading to tone.

  // publish potentiometer value..
  potVal.data = potread;
  potPub.publish(&potVal);


  nh.spinOnce();
  delay(10);

}
