#ifndef _ROS_prbt_hardware_support_FrameSpeeds_h
#define _ROS_prbt_hardware_support_FrameSpeeds_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"

namespace prbt_hardware_support
{

  class FrameSpeeds : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      uint32_t name_length;
      typedef char* _name_type;
      _name_type st_name;
      _name_type * name;
      uint32_t speed_length;
      typedef float _speed_type;
      _speed_type st_speed;
      _speed_type * speed;

    FrameSpeeds():
      header(),
      name_length(0), st_name(), name(nullptr),
      speed_length(0), st_speed(), speed(nullptr)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->name_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->name_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->name_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->name_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->name_length);
      for( uint32_t i = 0; i < name_length; i++){
      uint32_t length_namei = strlen(this->name[i]);
      varToArr(outbuffer + offset, length_namei);
      offset += 4;
      memcpy(outbuffer + offset, this->name[i], length_namei);
      offset += length_namei;
      }
      *(outbuffer + offset + 0) = (this->speed_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->speed_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->speed_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->speed_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->speed_length);
      for( uint32_t i = 0; i < speed_length; i++){
      offset += serializeAvrFloat64(outbuffer + offset, this->speed[i]);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      uint32_t name_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      name_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      name_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      name_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->name_length);
      if(name_lengthT > name_length)
        this->name = (char**)realloc(this->name, name_lengthT * sizeof(char*));
      name_length = name_lengthT;
      for( uint32_t i = 0; i < name_length; i++){
      uint32_t length_st_name;
      arrToVar(length_st_name, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_st_name; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_st_name-1]=0;
      this->st_name = (char *)(inbuffer + offset-1);
      offset += length_st_name;
        memcpy( &(this->name[i]), &(this->st_name), sizeof(char*));
      }
      uint32_t speed_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      speed_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      speed_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      speed_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->speed_length);
      if(speed_lengthT > speed_length)
        this->speed = (float*)realloc(this->speed, speed_lengthT * sizeof(float));
      speed_length = speed_lengthT;
      for( uint32_t i = 0; i < speed_length; i++){
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->st_speed));
        memcpy( &(this->speed[i]), &(this->st_speed), sizeof(float));
      }
     return offset;
    }

    virtual const char * getType() override { return "prbt_hardware_support/FrameSpeeds"; };
    virtual const char * getMD5() override { return "6bbf82485e72921292ce85ff0834aa55"; };

  };

}
#endif
