#ifndef _ROS_prbt_hardware_support_BrakeTestErrorCodes_h
#define _ROS_prbt_hardware_support_BrakeTestErrorCodes_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace prbt_hardware_support
{

  class BrakeTestErrorCodes : public ros::Msg
  {
    public:
      typedef int8_t _value_type;
      _value_type value;
      enum { STATUS_UNKNOWN =  0 };
      enum { STATUS_PERFORMING =  1 };
      enum { STATUS_SUCCESS =  2 };
      enum { STATUS_NO_SUCCESS =  3 };
      enum { STATUS_NO_CONTROL =  4 };
      enum { TRIGGER_BRAKETEST_SERVICE_FAILURE =  50 };
      enum { GET_NODE_NAMES_FAILURE =  51 };
      enum { GET_DURATION_FAILURE =  52 };
      enum { GET_STATUS_FAILURE =  53 };
      enum { START_BRAKE_TEST_FAILURE =  54 };
      enum { BRAKETEST_ALREADY_EXECUTING =  70 };
      enum { ROBOT_MOTION_DETECTED =  71 };
      enum { FAILURE =  99 };

    BrakeTestErrorCodes():
      value(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_value;
      u_value.real = this->value;
      *(outbuffer + offset + 0) = (u_value.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->value);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_value;
      u_value.base = 0;
      u_value.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->value = u_value.real;
      offset += sizeof(this->value);
     return offset;
    }

    virtual const char * getType() override { return "prbt_hardware_support/BrakeTestErrorCodes"; };
    virtual const char * getMD5() override { return "3d94689e95ce8a25b9e6555b0af70579"; };

  };

}
#endif
