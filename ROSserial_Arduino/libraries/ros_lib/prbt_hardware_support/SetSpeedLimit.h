#ifndef _ROS_SERVICE_SetSpeedLimit_h
#define _ROS_SERVICE_SetSpeedLimit_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace prbt_hardware_support
{

static const char SETSPEEDLIMIT[] = "prbt_hardware_support/SetSpeedLimit";

  class SetSpeedLimitRequest : public ros::Msg
  {
    public:
      typedef float _speed_limit_type;
      _speed_limit_type speed_limit;

    SetSpeedLimitRequest():
      speed_limit(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += serializeAvrFloat64(outbuffer + offset, this->speed_limit);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->speed_limit));
     return offset;
    }

    virtual const char * getType() override { return SETSPEEDLIMIT; };
    virtual const char * getMD5() override { return "c2b608fba3aaef5b68bff0c833d62d05"; };

  };

  class SetSpeedLimitResponse : public ros::Msg
  {
    public:

    SetSpeedLimitResponse()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
     return offset;
    }

    virtual const char * getType() override { return SETSPEEDLIMIT; };
    virtual const char * getMD5() override { return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class SetSpeedLimit {
    public:
    typedef SetSpeedLimitRequest Request;
    typedef SetSpeedLimitResponse Response;
  };

}
#endif
