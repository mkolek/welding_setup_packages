#ifndef _ROS_prbt_hardware_support_ModbusRegisterBlock_h
#define _ROS_prbt_hardware_support_ModbusRegisterBlock_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace prbt_hardware_support
{

  class ModbusRegisterBlock : public ros::Msg
  {
    public:
      typedef uint16_t _start_idx_type;
      _start_idx_type start_idx;
      uint32_t values_length;
      typedef uint16_t _values_type;
      _values_type st_values;
      _values_type * values;

    ModbusRegisterBlock():
      start_idx(0),
      values_length(0), st_values(), values(nullptr)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->start_idx >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->start_idx >> (8 * 1)) & 0xFF;
      offset += sizeof(this->start_idx);
      *(outbuffer + offset + 0) = (this->values_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->values_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->values_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->values_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->values_length);
      for( uint32_t i = 0; i < values_length; i++){
      *(outbuffer + offset + 0) = (this->values[i] >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->values[i] >> (8 * 1)) & 0xFF;
      offset += sizeof(this->values[i]);
      }
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      this->start_idx =  ((uint16_t) (*(inbuffer + offset)));
      this->start_idx |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->start_idx);
      uint32_t values_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      values_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      values_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      values_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->values_length);
      if(values_lengthT > values_length)
        this->values = (uint16_t*)realloc(this->values, values_lengthT * sizeof(uint16_t));
      values_length = values_lengthT;
      for( uint32_t i = 0; i < values_length; i++){
      this->st_values =  ((uint16_t) (*(inbuffer + offset)));
      this->st_values |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->st_values);
        memcpy( &(this->values[i]), &(this->st_values), sizeof(uint16_t));
      }
     return offset;
    }

    virtual const char * getType() override { return "prbt_hardware_support/ModbusRegisterBlock"; };
    virtual const char * getMD5() override { return "fe195cfcfc87bb5e9602528590f29a5d"; };

  };

}
#endif
