#ifndef _ROS_prbt_hardware_support_ModbusMsgInStamped_h
#define _ROS_prbt_hardware_support_ModbusMsgInStamped_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"
#include "std_msgs/UInt16MultiArray.h"
#include "std_msgs/Bool.h"

namespace prbt_hardware_support
{

  class ModbusMsgInStamped : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      typedef std_msgs::UInt16MultiArray _holding_registers_type;
      _holding_registers_type holding_registers;
      typedef std_msgs::Bool _disconnect_type;
      _disconnect_type disconnect;

    ModbusMsgInStamped():
      header(),
      holding_registers(),
      disconnect()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      offset += this->holding_registers.serialize(outbuffer + offset);
      offset += this->disconnect.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      offset += this->holding_registers.deserialize(inbuffer + offset);
      offset += this->disconnect.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return "prbt_hardware_support/ModbusMsgInStamped"; };
    virtual const char * getMD5() override { return "1023ba57fc2f9bcbdf47284c68c44a57"; };

  };

}
#endif
