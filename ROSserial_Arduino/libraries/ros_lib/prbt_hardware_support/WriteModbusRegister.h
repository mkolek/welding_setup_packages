#ifndef _ROS_SERVICE_WriteModbusRegister_h
#define _ROS_SERVICE_WriteModbusRegister_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "prbt_hardware_support/ModbusRegisterBlock.h"

namespace prbt_hardware_support
{

static const char WRITEMODBUSREGISTER[] = "prbt_hardware_support/WriteModbusRegister";

  class WriteModbusRegisterRequest : public ros::Msg
  {
    public:
      typedef prbt_hardware_support::ModbusRegisterBlock _holding_register_block_type;
      _holding_register_block_type holding_register_block;

    WriteModbusRegisterRequest():
      holding_register_block()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->holding_register_block.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->holding_register_block.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return WRITEMODBUSREGISTER; };
    virtual const char * getMD5() override { return "0851f7933b92fdd8b26651e23a384c83"; };

  };

  class WriteModbusRegisterResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;

    WriteModbusRegisterResponse():
      success(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
     return offset;
    }

    virtual const char * getType() override { return WRITEMODBUSREGISTER; };
    virtual const char * getMD5() override { return "358e233cde0c8a8bcfea4ce193f8fc15"; };

  };

  class WriteModbusRegister {
    public:
    typedef WriteModbusRegisterRequest Request;
    typedef WriteModbusRegisterResponse Response;
  };

}
#endif
