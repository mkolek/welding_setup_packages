#ifndef _ROS_SERVICE_GetOperationMode_h
#define _ROS_SERVICE_GetOperationMode_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "prbt_hardware_support/OperationModes.h"

namespace prbt_hardware_support
{

static const char GETOPERATIONMODE[] = "prbt_hardware_support/GetOperationMode";

  class GetOperationModeRequest : public ros::Msg
  {
    public:

    GetOperationModeRequest()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
     return offset;
    }

    virtual const char * getType() override { return GETOPERATIONMODE; };
    virtual const char * getMD5() override { return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class GetOperationModeResponse : public ros::Msg
  {
    public:
      typedef prbt_hardware_support::OperationModes _mode_type;
      _mode_type mode;

    GetOperationModeResponse():
      mode()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->mode.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->mode.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return GETOPERATIONMODE; };
    virtual const char * getMD5() override { return "1c0173954bf6ea81d34649fe96035dfa"; };

  };

  class GetOperationMode {
    public:
    typedef GetOperationModeRequest Request;
    typedef GetOperationModeResponse Response;
  };

}
#endif
