#ifndef _ROS_SERVICE_IsBrakeTestRequired_h
#define _ROS_SERVICE_IsBrakeTestRequired_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "pilz_msgs/IsBrakeTestRequiredResult.h"

namespace pilz_msgs
{

static const char ISBRAKETESTREQUIRED[] = "pilz_msgs/IsBrakeTestRequired";

  class IsBrakeTestRequiredRequest : public ros::Msg
  {
    public:

    IsBrakeTestRequiredRequest()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
     return offset;
    }

    virtual const char * getType() override { return ISBRAKETESTREQUIRED; };
    virtual const char * getMD5() override { return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class IsBrakeTestRequiredResponse : public ros::Msg
  {
    public:
      typedef pilz_msgs::IsBrakeTestRequiredResult _result_type;
      _result_type result;

    IsBrakeTestRequiredResponse():
      result()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->result.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->result.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return ISBRAKETESTREQUIRED; };
    virtual const char * getMD5() override { return "5ceabb78bf4fbbaf48fcb92c5a216329"; };

  };

  class IsBrakeTestRequired {
    public:
    typedef IsBrakeTestRequiredRequest Request;
    typedef IsBrakeTestRequiredResponse Response;
  };

}
#endif
