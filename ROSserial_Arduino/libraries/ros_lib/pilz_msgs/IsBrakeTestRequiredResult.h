#ifndef _ROS_pilz_msgs_IsBrakeTestRequiredResult_h
#define _ROS_pilz_msgs_IsBrakeTestRequiredResult_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pilz_msgs
{

  class IsBrakeTestRequiredResult : public ros::Msg
  {
    public:
      typedef int8_t _value_type;
      _value_type value;
      enum { NOT_REQUIRED =  -1 };
      enum { REQUIRED =  1 };
      enum { UNKNOWN =  0 };

    IsBrakeTestRequiredResult():
      value(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_value;
      u_value.real = this->value;
      *(outbuffer + offset + 0) = (u_value.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->value);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        int8_t real;
        uint8_t base;
      } u_value;
      u_value.base = 0;
      u_value.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->value = u_value.real;
      offset += sizeof(this->value);
     return offset;
    }

    virtual const char * getType() override { return "pilz_msgs/IsBrakeTestRequiredResult"; };
    virtual const char * getMD5() override { return "797bd67ef6461af956b7e7c0a3be1e83"; };

  };

}
#endif
