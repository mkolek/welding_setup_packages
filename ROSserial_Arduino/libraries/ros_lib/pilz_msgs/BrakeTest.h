#ifndef _ROS_SERVICE_BrakeTest_h
#define _ROS_SERVICE_BrakeTest_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pilz_msgs
{

static const char BRAKETEST[] = "pilz_msgs/BrakeTest";

  class BrakeTestRequest : public ros::Msg
  {
    public:

    BrakeTestRequest()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
     return offset;
    }

    virtual const char * getType() override { return BRAKETEST; };
    virtual const char * getMD5() override { return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class BrakeTestResponse : public ros::Msg
  {
    public:
      typedef bool _success_type;
      _success_type success;
      typedef const char* _error_msg_type;
      _error_msg_type error_msg;

    BrakeTestResponse():
      success(0),
      error_msg("")
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.real = this->success;
      *(outbuffer + offset + 0) = (u_success.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->success);
      uint32_t length_error_msg = strlen(this->error_msg);
      varToArr(outbuffer + offset, length_error_msg);
      offset += 4;
      memcpy(outbuffer + offset, this->error_msg, length_error_msg);
      offset += length_error_msg;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_success;
      u_success.base = 0;
      u_success.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->success = u_success.real;
      offset += sizeof(this->success);
      uint32_t length_error_msg;
      arrToVar(length_error_msg, (inbuffer + offset));
      offset += 4;
      for(unsigned int k= offset; k< offset+length_error_msg; ++k){
          inbuffer[k-1]=inbuffer[k];
      }
      inbuffer[offset+length_error_msg-1]=0;
      this->error_msg = (char *)(inbuffer + offset-1);
      offset += length_error_msg;
     return offset;
    }

    virtual const char * getType() override { return BRAKETEST; };
    virtual const char * getMD5() override { return "d006c48be24db1173a071ca9af4c8179"; };

  };

  class BrakeTest {
    public:
    typedef BrakeTestRequest Request;
    typedef BrakeTestResponse Response;
  };

}
#endif
