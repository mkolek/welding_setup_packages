#ifndef _ROS_SERVICE_GetSpeedOverride_h
#define _ROS_SERVICE_GetSpeedOverride_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace pilz_msgs
{

static const char GETSPEEDOVERRIDE[] = "pilz_msgs/GetSpeedOverride";

  class GetSpeedOverrideRequest : public ros::Msg
  {
    public:

    GetSpeedOverrideRequest()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
     return offset;
    }

    virtual const char * getType() override { return GETSPEEDOVERRIDE; };
    virtual const char * getMD5() override { return "d41d8cd98f00b204e9800998ecf8427e"; };

  };

  class GetSpeedOverrideResponse : public ros::Msg
  {
    public:
      typedef float _speed_override_type;
      _speed_override_type speed_override;

    GetSpeedOverrideResponse():
      speed_override(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += serializeAvrFloat64(outbuffer + offset, this->speed_override);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->speed_override));
     return offset;
    }

    virtual const char * getType() override { return GETSPEEDOVERRIDE; };
    virtual const char * getMD5() override { return "43aeeefed25ef5b00039c80b4d164a88"; };

  };

  class GetSpeedOverride {
    public:
    typedef GetSpeedOverrideRequest Request;
    typedef GetSpeedOverrideResponse Response;
  };

}
#endif
