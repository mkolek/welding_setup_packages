#ifndef _ROS_SERVICE_GetMotionSequence_h
#define _ROS_SERVICE_GetMotionSequence_h
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "moveit_msgs/MoveItErrorCodes.h"
#include "moveit_msgs/RobotTrajectory.h"
#include "moveit_msgs/RobotState.h"
#include "pilz_msgs/MotionSequenceRequest.h"

namespace pilz_msgs
{

static const char GETMOTIONSEQUENCE[] = "pilz_msgs/GetMotionSequence";

  class GetMotionSequenceRequest : public ros::Msg
  {
    public:
      typedef pilz_msgs::MotionSequenceRequest _commands_type;
      _commands_type commands;

    GetMotionSequenceRequest():
      commands()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->commands.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->commands.deserialize(inbuffer + offset);
     return offset;
    }

    virtual const char * getType() override { return GETMOTIONSEQUENCE; };
    virtual const char * getMD5() override { return "729ebbfec990c8eaa45188271e82611b"; };

  };

  class GetMotionSequenceResponse : public ros::Msg
  {
    public:
      typedef moveit_msgs::MoveItErrorCodes _error_code_type;
      _error_code_type error_code;
      uint32_t trajectory_start_length;
      typedef moveit_msgs::RobotState _trajectory_start_type;
      _trajectory_start_type st_trajectory_start;
      _trajectory_start_type * trajectory_start;
      uint32_t planned_trajectory_length;
      typedef moveit_msgs::RobotTrajectory _planned_trajectory_type;
      _planned_trajectory_type st_planned_trajectory;
      _planned_trajectory_type * planned_trajectory;
      typedef float _planning_time_type;
      _planning_time_type planning_time;

    GetMotionSequenceResponse():
      error_code(),
      trajectory_start_length(0), st_trajectory_start(), trajectory_start(nullptr),
      planned_trajectory_length(0), st_planned_trajectory(), planned_trajectory(nullptr),
      planning_time(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const override
    {
      int offset = 0;
      offset += this->error_code.serialize(outbuffer + offset);
      *(outbuffer + offset + 0) = (this->trajectory_start_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->trajectory_start_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->trajectory_start_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->trajectory_start_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->trajectory_start_length);
      for( uint32_t i = 0; i < trajectory_start_length; i++){
      offset += this->trajectory_start[i].serialize(outbuffer + offset);
      }
      *(outbuffer + offset + 0) = (this->planned_trajectory_length >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->planned_trajectory_length >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->planned_trajectory_length >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->planned_trajectory_length >> (8 * 3)) & 0xFF;
      offset += sizeof(this->planned_trajectory_length);
      for( uint32_t i = 0; i < planned_trajectory_length; i++){
      offset += this->planned_trajectory[i].serialize(outbuffer + offset);
      }
      offset += serializeAvrFloat64(outbuffer + offset, this->planning_time);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer) override
    {
      int offset = 0;
      offset += this->error_code.deserialize(inbuffer + offset);
      uint32_t trajectory_start_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      trajectory_start_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      trajectory_start_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      trajectory_start_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->trajectory_start_length);
      if(trajectory_start_lengthT > trajectory_start_length)
        this->trajectory_start = (moveit_msgs::RobotState*)realloc(this->trajectory_start, trajectory_start_lengthT * sizeof(moveit_msgs::RobotState));
      trajectory_start_length = trajectory_start_lengthT;
      for( uint32_t i = 0; i < trajectory_start_length; i++){
      offset += this->st_trajectory_start.deserialize(inbuffer + offset);
        memcpy( &(this->trajectory_start[i]), &(this->st_trajectory_start), sizeof(moveit_msgs::RobotState));
      }
      uint32_t planned_trajectory_lengthT = ((uint32_t) (*(inbuffer + offset))); 
      planned_trajectory_lengthT |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1); 
      planned_trajectory_lengthT |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2); 
      planned_trajectory_lengthT |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3); 
      offset += sizeof(this->planned_trajectory_length);
      if(planned_trajectory_lengthT > planned_trajectory_length)
        this->planned_trajectory = (moveit_msgs::RobotTrajectory*)realloc(this->planned_trajectory, planned_trajectory_lengthT * sizeof(moveit_msgs::RobotTrajectory));
      planned_trajectory_length = planned_trajectory_lengthT;
      for( uint32_t i = 0; i < planned_trajectory_length; i++){
      offset += this->st_planned_trajectory.deserialize(inbuffer + offset);
        memcpy( &(this->planned_trajectory[i]), &(this->st_planned_trajectory), sizeof(moveit_msgs::RobotTrajectory));
      }
      offset += deserializeAvrFloat64(inbuffer + offset, &(this->planning_time));
     return offset;
    }

    virtual const char * getType() override { return GETMOTIONSEQUENCE; };
    virtual const char * getMD5() override { return "3b6640804f52f7631e404e9ef94b1290"; };

  };

  class GetMotionSequence {
    public:
    typedef GetMotionSequenceRequest Request;
    typedef GetMotionSequenceResponse Response;
  };

}
#endif
